﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class Registration : MonoBehaviour
{
   public TMP_InputField nameField;
   public TMP_InputField passwordField;

   public Button submitButton;

   public void CallRegister()
   {
      StartCoroutine(Register());
   }

   IEnumerator Register()
   {
      WWWForm form = new WWWForm();
      form.AddField("name", nameField.text);
      form.AddField("password", passwordField.text);
      UnityWebRequest www = UnityWebRequest.Post("http://localhost/sqlconnect/register.php", form);
      yield return www.SendWebRequest();
      if (www.isNetworkError || www.isHttpError)
      {
         Debug.Log("User creation failed. Error #" + www);
      }
      else if(!www.isNetworkError || !www.isHttpError)
      {
         Debug.Log("User created successfully.");
         UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
      }
     
   }

   public void VerifyInputs()
   {
      submitButton.interactable = (nameField.text.Length >= 8 && passwordField.text.Length >= 8);
   } 
}
